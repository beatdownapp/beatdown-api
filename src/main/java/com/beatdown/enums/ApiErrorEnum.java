/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.enums;

public enum ApiErrorEnum {
    
    // User errors
    ERROR_1("error_1"), 
    ERROR_2("error_2"),
    ERROR_3("error_3"),
    ERROR_4("error_4"),
    
    // Offer errors
    ERROR_5("error_5"),
    ERROR_6("error_6"),
    ERROR_7("error_7"),
    
    // Personal offer errors
    ERROR_8("error_8")
    ;

    public String key;

    private ApiErrorEnum(String key) {
	this.key = key;
    }

}
