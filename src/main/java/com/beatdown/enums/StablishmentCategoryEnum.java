/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.enums;

import java.util.ArrayList;
import java.util.List;

public enum StablishmentCategoryEnum {
    
    // User errors
    HEALTH_BEAUTY(1), 
    TAKE_AWAY(2),
    ELECTRONICS(3),
    ESTHETIC(4),
    RESTAURANT(5),
    CLINIC(6),
    HOTEL(7),
    PROFESSIONAL(8),
    MOTOR(9),
    ACTIVITY(10),
    FASHION(11);
    
    public Integer key;

    private StablishmentCategoryEnum(Integer key) {
	this.key = key;
    }
    
    public static List<Integer> getList() {
	List<Integer> list = new ArrayList<>();
	
	list.add(HEALTH_BEAUTY.key);
	list.add(TAKE_AWAY.key);
	list.add(ELECTRONICS.key);
	list.add(ESTHETIC.key);
	list.add(RESTAURANT.key);
	list.add(CLINIC.key);
	list.add(HOTEL.key);
	list.add(PROFESSIONAL.key);
	list.add(MOTOR.key);
	list.add(ACTIVITY.key);
	list.add(FASHION.key);
	
	return list;
    }

}
