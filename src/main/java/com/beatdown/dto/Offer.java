/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.dto;

import java.math.BigDecimal;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * Offer
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-10T18:58:25.418Z")

public class Offer {
    @JsonProperty("offerId")
    private Integer offerId = null;

    @JsonProperty("titleCat")
    private String titleCat = null;

    @JsonProperty("titleSpa")
    private String titleSpa = null;

    @JsonProperty("titleEng")
    private String titleEng = null;

    @JsonProperty("imageUrl")
    private String imageUrl = null;

    @JsonProperty("discount")
    private Integer discount = null;

    @JsonProperty("stablishment")
    private Stablishment stablishment = null;

    @JsonProperty("favorite")
    private Boolean favorite = false;

    @JsonProperty("distance")
    private BigDecimal distance = null;

    @JsonProperty("offerType")
    private OfferType offerType = null;

    public Offer offerId(Integer offerId) {
	this.offerId = offerId;
	return this;
    }

    /**
     * Get offerId
     * 
     * @return offerId
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Integer getOfferId() {
	return offerId;
    }

    public void setOfferId(Integer offerId) {
	this.offerId = offerId;
    }

    public Offer titleCat(String titleCat) {
	this.titleCat = titleCat;
	return this;
    }

    /**
     * Get titleCat
     * 
     * @return titleCat
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getTitleCat() {
	return titleCat;
    }

    public void setTitleCat(String titleCat) {
	this.titleCat = titleCat;
    }

    public Offer titleSpa(String titleSpa) {
	this.titleSpa = titleSpa;
	return this;
    }

    /**
     * Get titleSpa
     * 
     * @return titleSpa
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getTitleSpa() {
	return titleSpa;
    }

    public void setTitleSpa(String titleSpa) {
	this.titleSpa = titleSpa;
    }

    public Offer titleEng(String titleEng) {
	this.titleEng = titleEng;
	return this;
    }

    /**
     * Get titleEng
     * 
     * @return titleEng
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getTitleEng() {
	return titleEng;
    }

    public void setTitleEng(String titleEng) {
	this.titleEng = titleEng;
    }

    public Offer imageUrl(String imageUrl) {
	this.imageUrl = imageUrl;
	return this;
    }

    /**
     * Get imageUrl
     * 
     * @return imageUrl
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getImageUrl() {
	return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
	this.imageUrl = imageUrl;
    }

    public Offer discount(Integer discount) {
	this.discount = discount;
	return this;
    }

    /**
     * Get discount
     * 
     * @return discount
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Integer getDiscount() {
	return discount;
    }

    public void setDiscount(Integer discount) {
	this.discount = discount;
    }

    public Offer stablishment(Stablishment stablishment) {
	this.stablishment = stablishment;
	return this;
    }

    /**
     * Get stablishment
     * 
     * @return stablishment
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid

    public Stablishment getStablishment() {
	return stablishment;
    }

    public void setStablishment(Stablishment stablishment) {
	this.stablishment = stablishment;
    }

    public Offer favorite(Boolean favorite) {
	this.favorite = favorite;
	return this;
    }

    /**
     * Get favorite
     * 
     * @return favorite
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Boolean isFavorite() {
	return favorite;
    }

    public void setFavorite(Boolean favorite) {
	this.favorite = favorite;
    }

    public Offer distance(BigDecimal distance) {
	this.distance = distance;
	return this;
    }

    /**
     * Get distance
     * 
     * @return distance
     **/
    @ApiModelProperty(value = "")

    @Valid

    public BigDecimal getDistance() {
	return distance;
    }

    public void setDistance(BigDecimal distance) {
	this.distance = distance;
    }

    public Offer offerType(OfferType offerType) {
	this.offerType = offerType;
	return this;
    }

    /**
     * Get offerType
     * 
     * @return offerType
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid

    public OfferType getOfferType() {
	return offerType;
    }

    public void setOfferType(OfferType offerType) {
	this.offerType = offerType;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	Offer offer = (Offer) o;
	return Objects.equals(this.offerId, offer.offerId) && Objects.equals(this.titleCat, offer.titleCat)
		&& Objects.equals(this.titleSpa, offer.titleSpa) && Objects.equals(this.titleEng, offer.titleEng)
		&& Objects.equals(this.imageUrl, offer.imageUrl) && Objects.equals(this.discount, offer.discount)
		&& Objects.equals(this.stablishment, offer.stablishment)
		&& Objects.equals(this.favorite, offer.favorite) && Objects.equals(this.distance, offer.distance)
		&& Objects.equals(this.offerType, offer.offerType);
    }

    @Override
    public int hashCode() {
	return Objects.hash(offerId, titleCat, titleSpa, titleEng, imageUrl, discount, stablishment, favorite, distance,
		offerType);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class Offer {\n");

	sb.append("    offerId: ").append(toIndentedString(offerId)).append("\n");
	sb.append("    titleCat: ").append(toIndentedString(titleCat)).append("\n");
	sb.append("    titleSpa: ").append(toIndentedString(titleSpa)).append("\n");
	sb.append("    titleEng: ").append(toIndentedString(titleEng)).append("\n");
	sb.append("    imageUrl: ").append(toIndentedString(imageUrl)).append("\n");
	sb.append("    discount: ").append(toIndentedString(discount)).append("\n");
	sb.append("    stablishment: ").append(toIndentedString(stablishment)).append("\n");
	sb.append("    favorite: ").append(toIndentedString(favorite)).append("\n");
	sb.append("    distance: ").append(toIndentedString(distance)).append("\n");
	sb.append("    offerType: ").append(toIndentedString(offerType)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
