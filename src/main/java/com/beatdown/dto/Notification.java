/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.dto;

import java.util.Date;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * Notification
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-03T17:37:36.459Z")

public class Notification {
    @JsonProperty("notificationId")
    private Integer notificationId = null;

    @JsonProperty("titleCat")
    private String titleCat = null;

    @JsonProperty("titleSpa")
    private String titleSpa = null;

    @JsonProperty("titleEng")
    private String titleEng = null;

    @JsonProperty("creationDate")
    private Date creationDate = null;

    public Notification notificationId(Integer notificationId) {
	this.notificationId = notificationId;
	return this;
    }

    /**
     * Get notificationId
     * 
     * @return notificationId
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Integer getNotificationId() {
	return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
	this.notificationId = notificationId;
    }

    public Notification titleCat(String titleCat) {
	this.titleCat = titleCat;
	return this;
    }

    /**
     * Get titleCat
     * 
     * @return titleCat
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getTitleCat() {
	return titleCat;
    }

    public void setTitleCat(String titleCat) {
	this.titleCat = titleCat;
    }

    public Notification titleSpa(String titleSpa) {
	this.titleSpa = titleSpa;
	return this;
    }

    /**
     * Get titleSpa
     * 
     * @return titleSpa
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getTitleSpa() {
	return titleSpa;
    }

    public void setTitleSpa(String titleSpa) {
	this.titleSpa = titleSpa;
    }

    public Notification titleEng(String titleEng) {
	this.titleEng = titleEng;
	return this;
    }

    /**
     * Get titleEng
     * 
     * @return titleEng
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getTitleEng() {
	return titleEng;
    }

    public void setTitleEng(String titleEng) {
	this.titleEng = titleEng;
    }

    public Notification creationDate(Date creationDate) {
	this.creationDate = creationDate;
	return this;
    }

    /**
     * Get creationDate
     * 
     * @return creationDate
     **/
    @ApiModelProperty(value = "")

    @Valid

    public Date getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(Date creationDate) {
	this.creationDate = creationDate;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	Notification notification = (Notification) o;
	return Objects.equals(this.notificationId, notification.notificationId)
		&& Objects.equals(this.titleCat, notification.titleCat)
		&& Objects.equals(this.titleSpa, notification.titleSpa)
		&& Objects.equals(this.titleEng, notification.titleEng)
		&& Objects.equals(this.creationDate, notification.creationDate);
    }

    @Override
    public int hashCode() {
	return Objects.hash(notificationId, titleCat, titleSpa, titleEng, creationDate);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class Notification {\n");

	sb.append("    notificationId: ").append(toIndentedString(notificationId)).append("\n");
	sb.append("    titleCat: ").append(toIndentedString(titleCat)).append("\n");
	sb.append("    titleSpa: ").append(toIndentedString(titleSpa)).append("\n");
	sb.append("    titleEng: ").append(toIndentedString(titleEng)).append("\n");
	sb.append("    creationDate: ").append(toIndentedString(creationDate)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
