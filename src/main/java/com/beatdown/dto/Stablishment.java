/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.dto;

import java.math.BigDecimal;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * Stablishment
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-04T19:32:08.635Z")

public class Stablishment
{
    @JsonProperty("stablishmentId")
    private Integer stablishmentId = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("category")
    private StablishmentCategory category = null;

    @JsonProperty("longitude")
    private BigDecimal longitude = null;

    @JsonProperty("latitude")
    private BigDecimal latitude = null;

    public Stablishment stablishmentId(Integer stablishmentId)
    {
        this.stablishmentId = stablishmentId;
        return this;
    }

    /**
     * Get stablishmentId
     * 
     * @return stablishmentId
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Integer getStablishmentId()
    {
        return stablishmentId;
    }

    public void setStablishmentId(Integer stablishmentId)
    {
        this.stablishmentId = stablishmentId;
    }

    public Stablishment name(String name)
    {
        this.name = name;
        return this;
    }

    /**
     * Get name
     * 
     * @return name
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Stablishment category(StablishmentCategory category)
    {
        this.category = category;
        return this;
    }

    /**
     * Get category
     * 
     * @return category
     **/
    @ApiModelProperty(value = "")

    @Valid

    public StablishmentCategory getCategory()
    {
        return category;
    }

    public void setCategory(StablishmentCategory category)
    {
        this.category = category;
    }

    public Stablishment longitude(BigDecimal longitude)
    {
        this.longitude = longitude;
        return this;
    }

    /**
     * Get longitude
     * 
     * @return longitude
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid

    public BigDecimal getLongitude()
    {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude)
    {
        this.longitude = longitude;
    }

    public Stablishment latitude(BigDecimal latitude)
    {
        this.latitude = latitude;
        return this;
    }

    /**
     * Get latitude
     * 
     * @return latitude
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid

    public BigDecimal getLatitude()
    {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude)
    {
        this.latitude = latitude;
    }

    @Override
    public boolean equals(java.lang.Object o)
    {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Stablishment stablishment = (Stablishment) o;
        return Objects.equals(this.stablishmentId, stablishment.stablishmentId)
            && Objects.equals(this.name, stablishment.name) && Objects.equals(this.category, stablishment.category)
            && Objects.equals(this.longitude, stablishment.longitude)
            && Objects.equals(this.latitude, stablishment.latitude);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(stablishmentId, name, category, longitude, latitude);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("class Stablishment {\n");

        sb.append("    stablishmentId: ").append(toIndentedString(stablishmentId)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    category: ").append(toIndentedString(category)).append("\n");
        sb.append("    longitude: ").append(toIndentedString(longitude)).append("\n");
        sb.append("    latitude: ").append(toIndentedString(latitude)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces (except the first line).
     */
    private String toIndentedString(java.lang.Object o)
    {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
