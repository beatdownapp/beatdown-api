/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * OfferType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-10T18:58:25.418Z")

public class OfferType {
    @JsonProperty("offerTypeId")
    private Integer offerTypeId = null;

    @JsonProperty("descriptionCat")
    private String descriptionCat = null;

    @JsonProperty("descriptionSpa")
    private String descriptionSpa = null;

    @JsonProperty("descriptionEng")
    private String descriptionEng = null;

    public OfferType offerTypeId(Integer offerTypeId) {
	this.offerTypeId = offerTypeId;
	return this;
    }

    /**
     * Get offerTypeId
     * 
     * @return offerTypeId
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Integer getOfferTypeId() {
	return offerTypeId;
    }

    public void setOfferTypeId(Integer offerTypeId) {
	this.offerTypeId = offerTypeId;
    }

    public OfferType descriptionCat(String descriptionCat) {
	this.descriptionCat = descriptionCat;
	return this;
    }

    /**
     * Get descriptionCat
     * 
     * @return descriptionCat
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getDescriptionCat() {
	return descriptionCat;
    }

    public void setDescriptionCat(String descriptionCat) {
	this.descriptionCat = descriptionCat;
    }

    public OfferType descriptionSpa(String descriptionSpa) {
	this.descriptionSpa = descriptionSpa;
	return this;
    }

    /**
     * Get descriptionSpa
     * 
     * @return descriptionSpa
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getDescriptionSpa() {
	return descriptionSpa;
    }

    public void setDescriptionSpa(String descriptionSpa) {
	this.descriptionSpa = descriptionSpa;
    }

    public OfferType descriptionEng(String descriptionEng) {
	this.descriptionEng = descriptionEng;
	return this;
    }

    /**
     * Get descriptionEng
     * 
     * @return descriptionEng
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getDescriptionEng() {
	return descriptionEng;
    }

    public void setDescriptionEng(String descriptionEng) {
	this.descriptionEng = descriptionEng;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	OfferType offerType = (OfferType) o;
	return Objects.equals(this.offerTypeId, offerType.offerTypeId)
		&& Objects.equals(this.descriptionCat, offerType.descriptionCat)
		&& Objects.equals(this.descriptionSpa, offerType.descriptionSpa)
		&& Objects.equals(this.descriptionEng, offerType.descriptionEng);
    }

    @Override
    public int hashCode() {
	return Objects.hash(offerTypeId, descriptionCat, descriptionSpa, descriptionEng);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class OfferType {\n");

	sb.append("    offerTypeId: ").append(toIndentedString(offerTypeId)).append("\n");
	sb.append("    descriptionCat: ").append(toIndentedString(descriptionCat)).append("\n");
	sb.append("    descriptionSpa: ").append(toIndentedString(descriptionSpa)).append("\n");
	sb.append("    descriptionEng: ").append(toIndentedString(descriptionEng)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
