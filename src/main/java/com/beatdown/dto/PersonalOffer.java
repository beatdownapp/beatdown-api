/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.dto;

import java.util.Date;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * PersonalOffer
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-04T19:32:08.635Z")

public class PersonalOffer
{
    @JsonProperty("personalOfferId")
    private Integer personalOfferId = null;

    @JsonProperty("code")
    private String code = null;

    @JsonProperty("expirationDate")
    private Date expirationDate = null;

    @JsonProperty("stablishment")
    private Stablishment stablishment = null;

    @JsonProperty("offer")
    private Offer offer = null;

    public PersonalOffer personalOfferId(Integer personalOfferId)
    {
        this.personalOfferId = personalOfferId;
        return this;
    }

    /**
     * Get personalOfferId
     * 
     * @return personalOfferId
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Integer getPersonalOfferId()
    {
        return personalOfferId;
    }

    public void setPersonalOfferId(Integer personalOfferId)
    {
        this.personalOfferId = personalOfferId;
    }

    public PersonalOffer code(String code)
    {
        this.code = code;
        return this;
    }

    /**
     * Get code
     * 
     * @return code
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public PersonalOffer expirationDate(Date expirationDate)
    {
        this.expirationDate = expirationDate;
        return this;
    }

    /**
     * Get expirationDate
     * 
     * @return expirationDate
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid

    public Date getExpirationDate()
    {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate)
    {
        this.expirationDate = expirationDate;
    }

    public PersonalOffer stablishment(Stablishment stablishment)
    {
        this.stablishment = stablishment;
        return this;
    }

    /**
     * Get stablishment
     * 
     * @return stablishment
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid

    public Stablishment getStablishment()
    {
        return stablishment;
    }

    public void setStablishment(Stablishment stablishment)
    {
        this.stablishment = stablishment;
    }

    public PersonalOffer offer(Offer offer)
    {
        this.offer = offer;
        return this;
    }

    /**
     * Get offer
     * 
     * @return offer
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid

    public Offer getOffer()
    {
        return offer;
    }

    public void setOffer(Offer offer)
    {
        this.offer = offer;
    }

    @Override
    public boolean equals(java.lang.Object o)
    {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PersonalOffer personalOffer = (PersonalOffer) o;
        return Objects.equals(this.personalOfferId, personalOffer.personalOfferId)
            && Objects.equals(this.code, personalOffer.code)
            && Objects.equals(this.expirationDate, personalOffer.expirationDate)
            && Objects.equals(this.stablishment, personalOffer.stablishment)
            && Objects.equals(this.offer, personalOffer.offer);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(personalOfferId, code, expirationDate, stablishment, offer);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("class PersonalOffer {\n");

        sb.append("    personalOfferId: ").append(toIndentedString(personalOfferId)).append("\n");
        sb.append("    code: ").append(toIndentedString(code)).append("\n");
        sb.append("    expirationDate: ").append(toIndentedString(expirationDate)).append("\n");
        sb.append("    stablishment: ").append(toIndentedString(stablishment)).append("\n");
        sb.append("    offer: ").append(toIndentedString(offer)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces (except the first line).
     */
    private String toIndentedString(java.lang.Object o)
    {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
