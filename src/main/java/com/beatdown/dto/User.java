/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * User
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-23T19:01:26.041Z")

public class User {
    @JsonProperty("userId")
    private Integer userId = null;

    @JsonProperty("email")
    private String email = null;

    @JsonProperty("password")
    private String password = null;

    @JsonProperty("avatar")
    private String avatar = null;

    public User userId(Integer userId) {
	this.userId = userId;
	return this;
    }

    /**
     * Get userId
     * 
     * @return userId
     **/
    @ApiModelProperty(value = "")

    public Integer getUserId() {
	return userId;
    }

    public void setUserId(Integer userId) {
	this.userId = userId;
    }

    public User email(String email) {
	this.email = email;
	return this;
    }

    /**
     * Get email
     * 
     * @return email
     **/
    @ApiModelProperty(value = "")

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public User password(String password) {
	this.password = password;
	return this;
    }

    /**
     * Get password
     * 
     * @return password
     **/
    @ApiModelProperty(required = false, value = "")
    @NotNull

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public User avatar(String avatar) {
	this.avatar = avatar;
	return this;
    }

    /**
     * Get avatar
     * 
     * @return avatar
     **/
    @ApiModelProperty(value = "")

    public String getAvatar() {
	return avatar;
    }

    public void setAvatar(String avatar) {
	this.avatar = avatar;
    }

    @Override
    public boolean equals(java.lang.Object o) {
	if (this == o) {
	    return true;
	}
	if (o == null || getClass() != o.getClass()) {
	    return false;
	}
	User user = (User) o;
	return Objects.equals(this.userId, user.userId) && Objects.equals(this.email, user.email)
		&& Objects.equals(this.password, user.password) && Objects.equals(this.avatar, user.avatar);
    }

    @Override
    public int hashCode() {
	return Objects.hash(userId, email, password, avatar);
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("class User {\n");

	sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
	sb.append("    email: ").append(toIndentedString(email)).append("\n");
	sb.append("    password: ").append(toIndentedString(password)).append("\n");
	sb.append("    avatar: ").append(toIndentedString(avatar)).append("\n");
	sb.append("}");
	return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
	if (o == null) {
	    return "null";
	}
	return o.toString().replace("\n", "\n    ");
    }
}
