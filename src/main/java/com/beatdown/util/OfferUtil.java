/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.util;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.beatdown.enums.ApiErrorEnum;
import com.beatdown.exceptions.ApiException;
import com.beatdown.model.OfferDb;
import com.beatdown.repositories.OfferRepository;

@Service
public class OfferUtil {

    @Autowired
    private OfferRepository offerRepo;

    public OfferDb getOfferDb(Integer offerId) {
	Optional<OfferDb> offer = offerRepo.findById(offerId);

	// En cas de no existir, retornem error
	if (!offer.isPresent()) {
	    String msg = String.format("Offer id '%s' not found.", offerId);
	    throw new ApiException(msg, ApiErrorEnum.ERROR_5.key, HttpStatus.BAD_REQUEST);
	}

	return offer.get();
    }

}
