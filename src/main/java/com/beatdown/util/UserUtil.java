/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.util;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.beatdown.enums.ApiErrorEnum;
import com.beatdown.exceptions.ApiException;
import com.beatdown.model.UserDb;
import com.beatdown.repositories.UserRepository;

@Service
public class UserUtil {
    
    @Autowired
    private UserRepository userRepo;
    
    public UserDb getUserDb(Integer userId) {
	Optional<UserDb> user = userRepo.findById(userId);

	// En cas de no existir, retornem error
	if (!user.isPresent()) {
	    String msg = String.format("User id '%s' not found.", userId);
	    throw new ApiException(msg, ApiErrorEnum.ERROR_2.key, HttpStatus.BAD_REQUEST);
	}
	
	return user.get();
    }

}
