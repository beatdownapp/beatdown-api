/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PasswordEncoderConfig {
    @Value("${passwordEncoder.strength}")
    private int passwordStrength;

    @Bean
    public PasswordEncoder passwordEncoder() {
	return new PasswordEncoder() {
	    @Override
	    public String encode(CharSequence rawPassword) {
		return new BCryptPasswordEncoder(passwordStrength).encode(rawPassword);
	    }

	    @Override
	    public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return new BCryptPasswordEncoder(passwordStrength).matches(rawPassword, encodedPassword);
	    }
	};
    }
}
