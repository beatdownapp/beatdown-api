/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.beatdown.api.StablishmentsApi;
import com.beatdown.dto.StablishmentCategory;
import com.beatdown.services.StablishmentService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-06T10:48:14.002Z")

@Controller
public class StablishmentsApiController implements StablishmentsApi {

    @Autowired
    private StablishmentService stablishmentService;

    public ResponseEntity<List<StablishmentCategory>> getAllCategories() {
	return stablishmentService.getAllCategories();
    }

}
