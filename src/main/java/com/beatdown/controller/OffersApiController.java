/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.beatdown.api.OffersApi;
import com.beatdown.dto.Offer;
import com.beatdown.dto.PersonalOffer;
import com.beatdown.services.OfferService;

import io.swagger.annotations.ApiParam;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-06T10:48:14.002Z")

@Controller
public class OffersApiController implements OffersApi {

    @Autowired
    private OfferService offerService;

    public ResponseEntity<List<PersonalOffer>> createPersonalOffer(@ApiParam(value = "",required=true) @PathVariable("userId") String userId,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "code", required = true) String code,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "latitude", required = true) BigDecimal latitude,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "longitude", required = true) BigDecimal longitude) {
	return offerService.createPersonalOffer(Integer.valueOf(userId), code, latitude, longitude);
    }

    public ResponseEntity<List<Offer>> getFavoriteOffers(@ApiParam(value = "",required=true) @PathVariable("userId") String userId,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "latitude", required = true) BigDecimal latitude,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "longitude", required = true) BigDecimal longitude) {
	return offerService.getFavoriteOffer(Integer.valueOf(userId), latitude, longitude);
    }

    public ResponseEntity<List<Offer>> getMapOffers(
	    @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "distance", required = true) Integer distance,
	    @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "latitude", required = true) BigDecimal latitude,
	    @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "longitude", required = true) BigDecimal longitude,
	    @ApiParam(value = "") @Valid @RequestParam(value = "stablishmentCategoryId", required = false) List<Integer> stablishmentCategoryId,
	    @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "userId", required = false) Integer userId) {
	return offerService.getMapOffers(distance, stablishmentCategoryId, latitude, longitude, userId);
    }

    public ResponseEntity<List<PersonalOffer>> getPersonalOffers(@ApiParam(value = "",required=true) @PathVariable("userId") String userId,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "latitude", required = true) BigDecimal latitude,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "longitude", required = true) BigDecimal longitude) {
	return offerService.getPersonalOffer(Integer.valueOf(userId), latitude, longitude);
    }

    public ResponseEntity<Void> markAsFavorite(
	    @ApiParam(value = "", required = true) @PathVariable("userId") String userId,
	    @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "offerId", required = true) Integer offerId) {
	return offerService.markAsFavorite(Integer.valueOf(userId), offerId);
    }
    
    public ResponseEntity<Void> deletePersonalOffer(@ApiParam(value = "",required=true) @PathVariable("userId") String userId,@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "personalOfferId", required = true) Integer personalOfferId) {
        return offerService.deletePersonalOffer(Integer.valueOf(userId), personalOfferId);
    }

}
