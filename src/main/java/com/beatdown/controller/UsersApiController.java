/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.controller;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.beatdown.api.UsersApi;
import com.beatdown.dto.User;
import com.beatdown.services.UserService;

import io.swagger.annotations.ApiParam;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-06T10:48:14.002Z")

@Controller
public class UsersApiController implements UsersApi {

    @Autowired
    private UserService userService;

    public ResponseEntity<?> createUser(@ApiParam(value = "", required = true) @Valid @RequestBody User body) {
	return userService.createUser(body);
    }

    public ResponseEntity<?> deleteUser(
	    @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "userId", required = true) Integer userId) {
	return userService.deleteUser(userId);
    }

    public ResponseEntity<?> editUser(
	    @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "userId", required = true) Integer userId,
	    @ApiParam(value = "", required = true) @Valid @RequestBody User body) {
	return userService.editUser(body, userId);
    }

    public ResponseEntity<?> login(
	    @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "email", required = true) String email,
	    @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "password", required = true) String password) {
	return userService.login(email, password);
    }

    public ResponseEntity<?> recoverAccount(
	    @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "email", required = true) String email,  @NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "locale", required = true) String locale) {
	return userService.recoverAccount(email, locale);
    }

}
