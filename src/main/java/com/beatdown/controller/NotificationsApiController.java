/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.beatdown.api.NotificationsApi;
import com.beatdown.dto.Notification;
import com.beatdown.services.NotificationService;

import io.swagger.annotations.ApiParam;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-06T11:01:18.613Z")

@Controller
public class NotificationsApiController implements NotificationsApi {

    @Autowired
    private NotificationService notificationService;

    public ResponseEntity<List<Notification>> getUserNotifications(
	    @ApiParam(value = "", required = true) @PathVariable("userId") String userId) {
	return notificationService.getUserNotifications(Integer.valueOf(userId));
    }

    public ResponseEntity<?> createNotification(
	    @ApiParam(value = "", required = true) @PathVariable("userId") String userId,
	    @ApiParam(value = "", required = true) @Valid @RequestBody Notification body) {
	return notificationService.createNotification(Integer.valueOf(userId), body);
    }

}
