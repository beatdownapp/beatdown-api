/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.converters;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.beatdown.dto.Stablishment;
import com.beatdown.model.StablishmentDb;

@Component
public class StablishmentDbToStablishment implements Converter<StablishmentDb, Stablishment> {
    
    @Autowired
    private StablishmentCategoryDbToStablishmentCategory category;
    
    @Override
    public Stablishment convert(StablishmentDb stablishmentDb) {
        return new Stablishment()
        	.stablishmentId(stablishmentDb.getStablishmentId())
        	.name(stablishmentDb.getName())
        	.category(category.convert(stablishmentDb.getStablishmentCategory()))
        	.longitude(BigDecimal.valueOf(stablishmentDb.getLongitude()))
        	.latitude(BigDecimal.valueOf(stablishmentDb.getLatitude()))
        	;
    }
    
}
