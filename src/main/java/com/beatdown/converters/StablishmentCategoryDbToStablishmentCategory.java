/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.beatdown.dto.StablishmentCategory;
import com.beatdown.model.StablishmentCategoryDb;

@Component
public class StablishmentCategoryDbToStablishmentCategory
	implements Converter<StablishmentCategoryDb, StablishmentCategory> {

    @Override
    public StablishmentCategory convert(StablishmentCategoryDb stablishmentCategoryDb) {
	return new StablishmentCategory()
		.descriptionCat(stablishmentCategoryDb.getDescriptionCat())
		.descriptionSpa(stablishmentCategoryDb.getDescriptionSpa())
		.descriptionEng(stablishmentCategoryDb.getDescriptionEng())
		.stablishmentCategoryId(stablishmentCategoryDb.getStablishmentCategoryId())
		;
    }
    
}
