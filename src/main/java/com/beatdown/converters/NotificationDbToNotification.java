/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.beatdown.dto.Notification;
import com.beatdown.model.NotificationDb;

@Component
public class NotificationDbToNotification
	implements Converter<NotificationDb, Notification> {

    @Override
    public Notification convert(NotificationDb notifDb) {
	return new Notification()
		.titleCat(notifDb.getTitleCat())
		.titleEng(notifDb.getTitleEng())
		.titleSpa(notifDb.getTitleEsp())
		.creationDate(notifDb.getCreationDate())
		.notificationId(notifDb.getNotificationId())
		;
    }
    
}
