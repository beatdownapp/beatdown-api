/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.beatdown.dto.OfferType;
import com.beatdown.model.OfferTypeDb;

@Component
public class OfferTypeDbToOfferType implements Converter<OfferTypeDb, OfferType> {
    
    @Override
    public OfferType convert(OfferTypeDb offerTypeDb) {
        return new OfferType()
        	.descriptionCat(offerTypeDb.getDescriptionCat())
        	.descriptionEng(offerTypeDb.getDescriptionEng())
        	.descriptionSpa(offerTypeDb.getDescriptionSpa())
        	.offerTypeId(offerTypeDb.getOfferTypeId());
    }
    
}
