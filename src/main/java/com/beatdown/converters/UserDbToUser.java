/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.beatdown.dto.User;
import com.beatdown.model.UserDb;

@Component
public class UserDbToUser implements Converter<UserDb, User> {
    
    @Override
    public User convert(UserDb userDb) {
        return new User()
            .userId(userDb.getUserId())
            .email(userDb.getEmail())
            .avatar(userDb.getAvatar());
    }
}
