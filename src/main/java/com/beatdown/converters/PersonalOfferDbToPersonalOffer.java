/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.beatdown.dto.PersonalOffer;
import com.beatdown.model.PersonalOfferDb;

@Component
public class PersonalOfferDbToPersonalOffer implements Converter<PersonalOfferDb, PersonalOffer> {
    
    @Autowired
    private StablishmentDbToStablishment stablishment;
    
    @Autowired
    private OfferDbToOffer offer;
    
    @Override
    public PersonalOffer convert(PersonalOfferDb personalOfferDb) {
        return new PersonalOffer()
        	.code(personalOfferDb.getCode())
        	.expirationDate(personalOfferDb.getExpirationDate())
        	.offer(offer.convert(personalOfferDb.getOffer()))
        	.personalOfferId(personalOfferDb.getPersonalOfferId())
        	.stablishment(stablishment.convert(personalOfferDb.getStablishment()))        	
        	;
    }
    
}
