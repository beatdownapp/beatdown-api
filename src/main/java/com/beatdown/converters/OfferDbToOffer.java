/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.beatdown.dto.Offer;
import com.beatdown.model.OfferDb;

@Component
public class OfferDbToOffer implements Converter<OfferDb, Offer> {
    
    @Autowired
    private StablishmentDbToStablishment stablishment;
    
    @Autowired
    private OfferTypeDbToOfferType offerType;
    
    @Override
    public Offer convert(OfferDb offerDb) {
        return new Offer()
        	.offerId(offerDb.getOfferId())
        	.titleCat(offerDb.getTitleCat())
        	.titleSpa(offerDb.getTitleSpa())
        	.titleEng(offerDb.getTitleEng())
        	.imageUrl("/static/image/" + offerDb.getImageUrl())
        	.discount(offerDb.getDiscount())
        	.stablishment(stablishment.convert(offerDb.getStablishment()))
        	.offerType(offerType.convert(offerDb.getOfferType()))
        	;
    }
    
}
