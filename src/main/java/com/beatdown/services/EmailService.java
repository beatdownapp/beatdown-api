/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.services;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;

    @Value("${emailService.senderEmail}")
    private String senderEmail;

    public void sendEmail(String msg, String subject, String to) {
	Context context = new Context();
	context.setVariable("user", "John Doe");

	MimeMessage mimeMessage = mailSender.createMimeMessage();
	try {
	    MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
	    message.setSubject(subject);
	    message.setFrom(senderEmail, "Beatdown APP");
	    message.setTo(to);
	    message.setText(msg, true);

	    mailSender.send(mimeMessage);
	} catch (MessagingException e) {
	    e.printStackTrace();
	} catch (UnsupportedEncodingException e) {
	    e.printStackTrace();
	}
    }
}
