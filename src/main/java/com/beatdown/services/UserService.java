/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.services;

import java.security.SecureRandom;
import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.beatdown.converters.UserDbToUser;
import com.beatdown.dto.User;
import com.beatdown.enums.ApiErrorEnum;
import com.beatdown.exceptions.ApiException;
import com.beatdown.model.UserDb;
import com.beatdown.repositories.FavoriteOfferRepository;
import com.beatdown.repositories.NotificationRepository;
import com.beatdown.repositories.PersonalOfferRepository;
import com.beatdown.repositories.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepo;
    
    @Autowired
    private NotificationRepository notifRepo;
    
    @Autowired
    private FavoriteOfferRepository favOfferRepo;
    
    @Autowired
    private PersonalOfferRepository personalOfferRepo;
    
    @Autowired
    private PasswordEncoder encoder;
    
    @Autowired
    private UserDbToUser userDbToUser;
    
    @Autowired
    private EmailService emailService;
    
    @Value("${passwordEncoder.strength}")
    private int passwordStrength;

    
    public ResponseEntity<?> createUser(User body) {

        // Comprovem que no estigui ja enregistrat
        if(userRepo.findByEmail(body.getEmail()).isPresent()) {
            String msg = String.format("User with email '%s' already exists.", body.getEmail());
            throw new ApiException(msg, ApiErrorEnum.ERROR_1.key, HttpStatus.BAD_REQUEST);
        }

        // Creem i guardem usuari
        UserDb userDb = userRepo.save(
            new UserDb()
            .creationDate(new Date())
            .email(body.getEmail())
            .password(encoder.encode(body.getPassword())));
        
        return new ResponseEntity<>(userDbToUser.convert(userDb), HttpStatus.CREATED);
    }
    
    public ResponseEntity<?> login(String email, String password) {
	
	Optional<UserDb> user = userRepo.findByEmail(email);
	
	// Comprovem que estigui ja enregistrat
	if(!user.isPresent()) {
	    String msg = String.format("User with email '%s' not found.", email);
	    throw new ApiException(msg, ApiErrorEnum.ERROR_3.key, HttpStatus.BAD_REQUEST);
	}
	
	// Obtenim usuari
	UserDb userDb = user.get();
	
	// Comprovem contrasenya
	if (!encoder.matches(password, userDb.getPassword()))
	    throw new ApiException("Password's not correct", ApiErrorEnum.ERROR_4.key, HttpStatus.BAD_REQUEST);
	
	return new ResponseEntity<>(userDbToUser.convert(userDb), HttpStatus.CREATED);
    }
    
    public ResponseEntity<?> deleteUser(Integer userId) {
        
	// Comprovem usuari
	UserDb user = checkUserId(userId);
        
	// Eliminem primer les referències
	notifRepo.deleteAllByUser(user);
	favOfferRepo.deleteAllByUser(user);
	personalOfferRepo.deleteAllByUser(user);
	
        // Eliminem l'usuari
        userRepo.delete(user);
        
        
        
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    public ResponseEntity<?> editUser(User body, Integer userId) {
	
	// Comprovem usuari
	UserDb user = checkUserId(userId);
	
	if (body.getPassword() != null && !body.getPassword().isEmpty())
	    user.password(encoder.encode(body.getPassword()));
	
	// Creem i guardem usuari
	user = userRepo.save(
        	user
        	.modificationDate(new Date())
                .avatar(body.getAvatar()));

        return new ResponseEntity<>(userDbToUser.convert(user), HttpStatus.CREATED);
    }
    
    public ResponseEntity<?> recoverAccount(String email, String locale) {
	
	Optional<UserDb> user = userRepo.findByEmail(email);
	
	// Comprovem que estigui ja enregistrat
	if(!user.isPresent()) {
	    String msg = String.format("User with email '%s' not found.", email);
	    throw new ApiException(msg, ApiErrorEnum.ERROR_3.key, HttpStatus.BAD_REQUEST);
	}
	
	// Db
	UserDb userDb = user.get();
	
	// Generem nova password
	char[] possibleCharacters = (new String(
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?"))
			.toCharArray();
	String randomStr = RandomStringUtils.random(passwordStrength, 0, possibleCharacters.length - 1, false, false,
		possibleCharacters, new SecureRandom());
	
	// Codifiquem i la guardem
	userRepo.save(userDb.password(encoder.encode(randomStr)));
	
	// Missatge
	String msg = "";
	String subject = "";

	if (locale.equalsIgnoreCase("es")) {
	    msg = String.format(
		    "La constraseña de tu cuenta se ha restablecido.<br><br>Por favor, entra con esta nueva contraseña <b>%s</b> y cámbiala dentro de la app.<br><br><br>Atentamente,<br><i>Equipo de Beatdown</i>",
		    randomStr);
	    subject = "Recuperar cuenta";
	} else if (locale.equalsIgnoreCase("en")) {
	    msg = String.format(
		    "The password of your account has been restored.<br><br>Please, log in with this new password <b>%s</b> and change it into the app.<br><br><br>Best regards,<br><i>Beatdown Team</i>",
		    randomStr);
	    subject = "Recover account";
	} else {
	    msg = String.format(
		    "La constrasenya del teu compte s'ha reestablert.<br><br>Si us plau, entra amb aquesta nova contrasenya <b>%s</b> i modifica-la a dins de l'app.<br><br><br>Atentament,<br><i>Equip de Beatdown</i>",
		    randomStr);
	    subject = "Recuperar compte";
	}

	// Enviem mail
	emailService.sendEmail(msg, subject, email);

        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    /**
     * Comprova que existeix l'ID de l'usuari a la base de dades.
     * @param userId
     */
    private UserDb checkUserId(Integer userId) {
	Optional<UserDb> user = userRepo.findById(userId);

	if (!user.isPresent()) {
	    String msg = String.format("User id '%s' not found.", userId);
	    throw new ApiException(msg, ApiErrorEnum.ERROR_2.key, HttpStatus.BAD_REQUEST);
	}

	return user.get();
    }

}
