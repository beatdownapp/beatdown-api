/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.beatdown.converters.OfferDbToOffer;
import com.beatdown.converters.PersonalOfferDbToPersonalOffer;
import com.beatdown.dto.Offer;
import com.beatdown.dto.PersonalOffer;
import com.beatdown.enums.ApiErrorEnum;
import com.beatdown.enums.StablishmentCategoryEnum;
import com.beatdown.exceptions.ApiException;
import com.beatdown.model.FavoriteOfferDb;
import com.beatdown.model.OfferDb;
import com.beatdown.model.PersonalOfferDb;
import com.beatdown.model.UserDb;
import com.beatdown.repositories.FavoriteOfferRepository;
import com.beatdown.repositories.OfferRepository;
import com.beatdown.repositories.PersonalOfferRepository;
import com.beatdown.util.OfferUtil;
import com.beatdown.util.UserUtil;

@Service
public class OfferService {

    /** Repositoris **/
    @Autowired
    private OfferRepository offerRepo;
    
    @Autowired
    private FavoriteOfferRepository favoriteOfferRepo;

    @Autowired
    private PersonalOfferRepository personalOfferRepo;

    /** Conversors **/
    @Autowired
    private OfferDbToOffer offerDbToOffer;

    @Autowired
    private PersonalOfferDbToPersonalOffer personalOfferDbToPersonalOffer;

    /** Útils **/
    @Autowired
    private UserUtil userUtil;

    @Autowired
    private OfferUtil offerUtil;

    /**
     * Retorna les ofertes favorites de l'usuari.
     * 
     * @param userId
     * @return
     */
    public ResponseEntity<List<Offer>> getFavoriteOffer(Integer userId, BigDecimal latitude, BigDecimal longitude) {

	// Recuperem ofertes favorites de l'usuari
	List<FavoriteOfferDb> favoriteOffers = favoriteOfferRepo.findByUser(userUtil.getUserDb(userId));

	// Convertim
	List<Offer> offerList = new ArrayList<>();

	favoriteOffers.forEach(favOffer -> 
	{
	    Offer offer = offerDbToOffer.convert(favOffer.getOffer());
	    offer.distance(offerRepo.getOfferDistance(latitude, longitude, offer.getStablishment().getStablishmentId()).setScale(2, RoundingMode.CEILING));
	    offer.favorite(true);
	    offerList.add(offer);
	});

	return new ResponseEntity<>(offerList, HttpStatus.OK);
    }

    /**
     * Retorna les ofertes favorites de l'usuari.
     * 
     * @param userId
     * @return
     */
    public ResponseEntity<List<PersonalOffer>> getPersonalOffer(Integer userId, BigDecimal latitude, BigDecimal longitude) {

	// Recuperem ofertes personals de l'usuari
	List<PersonalOfferDb> personalOffers = personalOfferRepo
		.findByUserAndUsedIsFalseAndExpirationDateGreaterThan(userUtil.getUserDb(userId), new Date());

	// Convertim
	List<PersonalOffer> offerList = new ArrayList<>();

	personalOffers.forEach(persOffer -> { 	    
	    PersonalOffer offer = personalOfferDbToPersonalOffer.convert(persOffer);
	    offer.getOffer().distance(offerRepo.getOfferDistance(latitude, longitude, persOffer.getStablishment().getStablishmentId()).setScale(2, RoundingMode.CEILING));
	    offerList.add(offer);
	    });

	return new ResponseEntity<>(offerList, HttpStatus.OK);
    }

    /**
     * Retorna un llistat de totes les ofertes donats els paràmetres.
     * 
     * @param distance
     * @param stablishmentCategoryId
     * @param latitude
     * @param longitude
     * @param userId
     * @return
     */
    public ResponseEntity<List<Offer>> getMapOffers(Integer distance, List<Integer> stablishmentCategoryId,
	    BigDecimal latitude, BigDecimal longitude, Integer userId) {

	// Si les categories venen buides, busquem totes
	if (stablishmentCategoryId == null) {
	    stablishmentCategoryId = StablishmentCategoryEnum.getList();
	}

	// Si ens ve 0, posem com a mínim 1 km
	if (distance == 0)
	    distance = 1;
	
	// Recuperem ofertes
	List<Object[]> stablishmentsByDistance = offerRepo.findAllOffersByDistance(distance, latitude, longitude,
		stablishmentCategoryId, userId);

	// Convertim
	List<Offer> offerList = new ArrayList<>();
	Offer offer;

	for (Object[] stablishmentByDistance : stablishmentsByDistance) {

	    // Recuperem oferta associada
	    Optional<OfferDb> offerOpt = offerRepo.findById((Integer) stablishmentByDistance[3]);

	    if (offerOpt.isPresent()) {

		// Afegim
		offer = offerDbToOffer.convert(offerOpt.get());

		// Ajustem distància
		offer.distance(BigDecimal.valueOf((Double) stablishmentByDistance[0]).setScale(2, RoundingMode.CEILING));

		// Si hi és, el marquem com a favorit
		offer.favorite(stablishmentByDistance[4] != null);

		offerList.add(offer);

	    }

	}

	return new ResponseEntity<>(offerList, HttpStatus.OK);
    }

    /**
     * Marca una oferta com a favorita per un usuari donat.
     * 
     * @param userId
     * @param offerId
     * @return
     */
    public ResponseEntity<Void> markAsFavorite(Integer userId, Integer offerId) {

	// Comprovem primer de tot si ja existeix marcat
	Optional<FavoriteOfferDb> favOffer = favoriteOfferRepo.findByUserAndOffer(userUtil.getUserDb(userId),
		offerUtil.getOfferDb(offerId));

	// En cas que no
	if (!favOffer.isPresent())
	    // Guardem la combinació d'oferta i usuari a la taula
	    favoriteOfferRepo
		    .save(new FavoriteOfferDb().offer(offerUtil.getOfferDb(offerId)).user(userUtil.getUserDb(userId)));

	// En cas que sí
	else
	    // L'eliminem
	    favoriteOfferRepo.delete(favOffer.get());

	return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Crea una oferta personal per l'usuari donat.
     * 
     * @param userId
     * @param code
     * @return
     */
    public ResponseEntity<List<PersonalOffer>> createPersonalOffer(Integer userId, String code, BigDecimal latitude, BigDecimal longitude) {

	// Comprovem primer que el codi tingui la mida adequada
	if (code.length() > 5) {
	    String msg = String.format("QR code '%s' not valid. Size = %s", code, code.length());
	    throw new ApiException(msg, ApiErrorEnum.ERROR_6.key, HttpStatus.BAD_REQUEST);
	}

	// Comprovem que existeixi a la bd abans de continuar
	Optional<OfferDb> offer = offerRepo.findByCode(code);

	if (!offer.isPresent()) {
	    String msg = String.format("Offer not found by QR code '%s'", code);
	    throw new ApiException(msg, ApiErrorEnum.ERROR_5.key, HttpStatus.BAD_REQUEST);
	}

	UserDb user = userUtil.getUserDb(userId);

	// Comprovem si ja el té enregistrat
	Optional<PersonalOfferDb> persOffer = personalOfferRepo.findByUserAndCode(userUtil.getUserDb(userId), code);

	// En cas que no
	if (!persOffer.isPresent()) {

	    // Calendar
	    Calendar cal = Calendar.getInstance();

	    Date creationDate = cal.getTime();

	    // Augmentem 1 mes per data caducitat
	    cal.setTime(creationDate);
	    cal.add(Calendar.MONTH, 1);

	    Date expirationDate = cal.getTime();

	    // Guardem la combinació d'oferta i codi
	    personalOfferRepo
		    .save(new PersonalOfferDb().code(code).creationDate(creationDate).expirationDate(expirationDate)
			    .offer(offer.get()).stablishment(offer.get().getStablishment()).user(user).used(false));

	    // Un cop creat, retornem el llistat complet
	    return this.getPersonalOffer(userId, latitude, longitude);
	}
	// En cas que sí
	else {
	    String msg = String.format("User id '%s' has already personal offer with QR code '%s'", userId, code);
	    throw new ApiException(msg, ApiErrorEnum.ERROR_7.key, HttpStatus.BAD_REQUEST);
	}

    }

    /**
     * Marca una oferta personal com usada.
     * 
     * @param userId
     * @param personalOfferId
     * @return
     */
    public ResponseEntity<Void> deletePersonalOffer(Integer userId, Integer personalOfferId) {

	// Comprovem primer de tot si ja existeix marcat
	Optional<PersonalOfferDb> persOffer = personalOfferRepo.findByUserAndPersonalOfferId(userUtil.getUserDb(userId),
		personalOfferId);

	// En cas que no
	if (!persOffer.isPresent()) {
	    String msg = String.format("User id '%s' has no personal offer with id '%s'", userId, personalOfferId);
	    throw new ApiException(msg, ApiErrorEnum.ERROR_8.key, HttpStatus.BAD_REQUEST);
	}

	// En cas que sí
	else
	    // El marquem com usat
	    personalOfferRepo.save(persOffer.get().used(true));

	return new ResponseEntity<>(HttpStatus.OK);
    }

}
