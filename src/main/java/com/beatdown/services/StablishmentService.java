/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.beatdown.converters.StablishmentCategoryDbToStablishmentCategory;
import com.beatdown.dto.StablishmentCategory;
import com.beatdown.model.StablishmentCategoryDb;
import com.beatdown.repositories.StablishmentCategoryRepository;

@Service
public class StablishmentService {

    @Autowired
    private StablishmentCategoryRepository stablishmentCategoryRepo;

    @Autowired
    private StablishmentCategoryDbToStablishmentCategory stablishmentCategoryDbToStablishmentCategory;

    /**
     * Retorna totes les categories d'establiments.
     * 
     * @return
     */
    public ResponseEntity<List<StablishmentCategory>> getAllCategories() {

	// Recuperem totes les categories
	Iterable<StablishmentCategoryDb> allCategories = stablishmentCategoryRepo.findAll();

	// Convertim
	List<StablishmentCategory> categoryList = new ArrayList<>();

	allCategories
		.forEach(category -> categoryList.add(stablishmentCategoryDbToStablishmentCategory.convert(category)));

	return new ResponseEntity<>(categoryList, HttpStatus.OK);
    }

}
