/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.beatdown.converters.NotificationDbToNotification;
import com.beatdown.dto.Notification;
import com.beatdown.exceptions.ApiException;
import com.beatdown.firebase.AndroidPushNotificationService;
import com.beatdown.model.NotificationDb;
import com.beatdown.repositories.NotificationRepository;
import com.beatdown.util.UserUtil;

@Service
public class NotificationService {

    @Autowired
    private NotificationRepository notifRepo;

    @Autowired
    private NotificationDbToNotification notifDbToNotif;

    @Autowired
    private UserUtil userUtil;

    @Autowired
    private AndroidPushNotificationService pushService;

    /**
     * Retorna totes les notificacions donat l'usuari.
     * 
     * @param userId
     * @return
     */
    public ResponseEntity<List<Notification>> getUserNotifications(Integer userId) {

	// Recuperem totes les notificacions
	List<NotificationDb> notifications = notifRepo.findByUser(userUtil.getUserDb(userId));

	// Convertim
	List<Notification> notificationList = new ArrayList<>();

	notifications.forEach(notif -> notificationList.add(notifDbToNotif.convert(notif)));

	return new ResponseEntity<>(notificationList, HttpStatus.OK);
    }

    /**
     * Crea una nova notificació per un usuari donat.
     * 
     * @param userId
     * @param body
     * @return
     */
    public ResponseEntity<?> createNotification(Integer userId, Notification body) {

	// Preparem notificació
	NotificationDb notification = new NotificationDb().titleCat(body.getTitleCat()).titleEng(body.getTitleEng())
		.titleEsp(body.getTitleSpa()).user(userUtil.getUserDb(userId)).creationDate(new Date());

	// Enviem push
	sendPush(notification);

	// Creem la notificació
	notifRepo.save(notification);

	return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Envia notificació push.
     * 
     * @param notification
     * @return
     */
    private ResponseEntity<?> sendPush(NotificationDb notification) {

	JSONObject body = new JSONObject();
	body.put("to", "/topics/app");
	body.put("priority", "high");

	JSONObject notification_ = new JSONObject();
	notification_.put("title", "action_msg_new_notification");
	JSONObject body_ = new JSONObject();
	body_.put("ca", notification.getTitleCat());
	body_.put("es", notification.getTitleEsp());
	body_.put("en", notification.getTitleEng());
	notification_.put("body", body_.toString());

	body.put("notification", notification_);
	// TODO Podem afegir "data" per dades personalitzades!

	HttpEntity<String> request = new HttpEntity<>(body.toString());

	// Envien notificació push
	CompletableFuture<String> pushNotification = pushService.send(request);
	CompletableFuture.allOf(pushNotification).join();

	try {
	    String firebaseResponse = pushNotification.get();

	    return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new ApiException(e.getMessage(), "", HttpStatus.BAD_REQUEST);
	}

    }

}
