/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.beatdown.model.FavoriteOfferDb;
import com.beatdown.model.OfferDb;
import com.beatdown.model.UserDb;

@Repository
public interface FavoriteOfferRepository extends CrudRepository<FavoriteOfferDb, Integer> {

    List<FavoriteOfferDb> findByUser(UserDb userDb);

    Optional<FavoriteOfferDb> findByUserAndOffer(UserDb user, OfferDb offer);
    
    @Transactional
    void deleteAllByUser(UserDb user);

}
