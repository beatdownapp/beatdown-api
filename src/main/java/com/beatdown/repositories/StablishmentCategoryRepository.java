/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.beatdown.model.StablishmentCategoryDb;

@Repository
public interface StablishmentCategoryRepository extends CrudRepository<StablishmentCategoryDb, Integer> {

}
