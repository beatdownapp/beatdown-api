/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.repositories;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.beatdown.model.OfferDb;
import com.beatdown.model.StablishmentDb;

@Repository
public interface OfferRepository extends CrudRepository<OfferDb, Integer> {

    /*
     select
	( 6371 * acos( cos(radians(:latitude)) * cos(radians(x(s.location))) * cos(radians(y(s.location)) - radians(:longitude)) + sin(radians(:latitude)) * sin(radians(x(s.location))) ) ) as distance,
	s.stablishment_category_id,
	o.stablishment_id,
	o.offer_id as offer,
	fo.offer_id as favoffer,
	o.offer_type_id
    from
    	beatdown.offer o
    join beatdown.stablishment s on
    	s.stablishment_id = o.stablishment_id
    left join beatdown.favorite_offer fo on
    	fo.offer_id = o.offer_id
    	and fo.user_id = :userId
    having
    	distance < :distance
    	and stablishment_category_id in (:stablishmentCategory)
    	and o.offer_type_id not in (3)
    order by
    	distance;
     */
    /**
     * Retorna el llistat d'ofertes per distància i coordenades.
     * 
     * @param distance
     * @param latitude
     * @param longitude
     * @param stablishmentCategory
     * @param userId
     * @return
     */
    @Query(nativeQuery = true, value = " select\n" + 
    	"	( 6371 * acos( cos(radians(:latitude)) * cos(radians(x(s.location))) * cos(radians(y(s.location)) - radians(:longitude)) + sin(radians(:latitude)) * sin(radians(x(s.location))) ) ) as distance,\n" + 
    	"	s.stablishment_category_id,\n" + 
    	"	o.stablishment_id,\n" + 
    	"	o.offer_id as offer,\n" + 
    	"	fo.offer_id as favoffer,\n" + 
    	"	o.offer_type_id\n" + 
    	"    from\n" + 
    	"    	beatdown.offer o\n" + 
    	"    join beatdown.stablishment s on\n" + 
    	"    	s.stablishment_id = o.stablishment_id\n" + 
    	"    left join beatdown.favorite_offer fo on\n" + 
    	"    	fo.offer_id = o.offer_id\n" + 
    	"    	and fo.user_id = :userId\n" + 
    	"    having\n" + 
    	"    	distance < :distance\n" + 
    	"    	and stablishment_category_id in (:stablishmentCategory)\n" + 
    	"    	and o.offer_type_id not in (3)\n" + 
    	"    order by\n" + 
    	"    	distance;")
    List<Object[]> findAllOffersByDistance(@Param("distance") Integer distance, @Param("latitude") BigDecimal latitude,
	    @Param("longitude") BigDecimal longitude, @Param("stablishmentCategory") List<Integer> stablishmentCategory,
	    @Param("userId") Integer userId);

    @Query(nativeQuery = true, value = "select\n" + 
    	"	( 6371 * acos( cos(radians(:latitude)) * cos(radians(x(s.location))) * cos(radians(y(s.location)) - radians(:longitude)) + sin(radians(:latitude)) * sin(radians(x(s.location))) ) ) as distance\n" + 
    	"from\n" + 
    	"	beatdown.stablishment s\n" + 
    	"where\n" + 
    	"	s.stablishment_id = :stablishmentId")
    BigDecimal getOfferDistance(@Param("latitude") BigDecimal latitude, @Param("longitude") BigDecimal longitude, @Param("stablishmentId") Integer stablishmentId);
    
    /**
     * Busca ofertes per establiment.
     * 
     * @param stablishmentDb
     * @return
     */
    Optional<OfferDb> findByStablishment(StablishmentDb stablishmentDb);
    
    /**
     * Busca oferta per codi.
     * @param code
     * @return
     */
    Optional<OfferDb> findByCode(String code);

}
