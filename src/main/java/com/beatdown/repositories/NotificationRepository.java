/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.beatdown.model.NotificationDb;
import com.beatdown.model.UserDb;

@Repository
public interface NotificationRepository extends CrudRepository<NotificationDb, Integer> {

    List<NotificationDb> findByUser(UserDb user);
    
    @Transactional
    void deleteAllByUser(UserDb user);

}
