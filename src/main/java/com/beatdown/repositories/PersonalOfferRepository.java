/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.repositories;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.beatdown.model.PersonalOfferDb;
import com.beatdown.model.UserDb;

@Repository
public interface PersonalOfferRepository extends CrudRepository<PersonalOfferDb, Integer> {

    List<PersonalOfferDb> findByUserAndUsedIsFalseAndExpirationDateGreaterThan(UserDb userDb, Date now);

    Optional<PersonalOfferDb> findByUserAndCode(UserDb user, String code);

    Optional<PersonalOfferDb> findByUserAndPersonalOfferId(UserDb user, Integer id);
    
    @Transactional
    void deleteAllByUser(UserDb user);

}
