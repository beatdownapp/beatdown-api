/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.beatdown.model.UserDb;

@Repository
public interface UserRepository extends CrudRepository<UserDb, Integer> {

    Optional<UserDb> findByEmailAndPassword(String email, String password);

    Optional<UserDb> findByEmail(String email);

}
