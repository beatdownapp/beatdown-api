/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.exceptions;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("serial")
public class ApiException extends RuntimeException
{

    private String errorCode;

    private String errorMessage;

    private HttpStatus status;

    public ApiException(Throwable throwable)
    {
        super(throwable);
    }

    public ApiException(String msg, Throwable throwable)
    {
        super(msg, throwable);
    }

    public ApiException(String msg)
    {
        super(msg);
    }

    public ApiException(String message, String errorCode, HttpStatus status)
    {
        super();
        this.errorCode = errorCode;
        this.errorMessage = message;
        this.status = status;
    }

    public void setErrorCode(String errorCode)
    {
        this.errorCode = errorCode;
    }

    public String getErrorCode()
    {
        return errorCode;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    @Override
    public String toString()
    {
        return this.errorCode + " : " + this.getErrorMessage();
    }

    public HttpStatus getStatus()
    {
        return status;
    }

    public void setStatus(HttpStatus status)
    {
        this.status = status;
    }

    public String toJson()
    {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException();
        }
    }
}
