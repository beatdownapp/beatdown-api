/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestErrorHandler
{

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ApiError> processValidationError(ApiException ex)
    {
        return new ResponseEntity<>(new ApiError(ex.getErrorMessage(), ex.getErrorCode()), ex.getStatus());
    }
}
