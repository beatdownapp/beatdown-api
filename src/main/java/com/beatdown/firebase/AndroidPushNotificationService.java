/*******************************************************************************
 * Copyright (C) 2019 Andreu van Walré Fernández.
 * All rights reserved.
 ******************************************************************************/
package com.beatdown.firebase;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Servei encarregat d'enviar notificacions push als dispositius.
 * @author Andreu
 *
 */
@Service
public class AndroidPushNotificationService {

    @Value("${firebase.serverKey}")
    private String firebaseKey;

    @Value("${firebase.url}")
    private String firebaseUrl;

    @Async
    public CompletableFuture<String> send(HttpEntity<String> entity) {

	RestTemplate restTemplate = new RestTemplate();

	ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
	interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + firebaseKey));
	interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
	restTemplate.setInterceptors(interceptors);

	String firebaseResponse = restTemplate.postForObject(firebaseUrl, entity, String.class);

	return CompletableFuture.completedFuture(firebaseResponse);
    }
}
